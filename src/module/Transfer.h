#pragma once
#include "define.h"

class Transfer {
  private:
    String id;
    double inTemp;
    double outTemp;
    double humidity;
    bool LeftDoor;
    bool RightDoor;
    bool isInsert;
    int user;

  public:
    Transfer(double inTemp, double outTemp, double humidity, bool LeftDoor, bool RightDoor) {
        this->id = ESP32_ID;
        this->inTemp = inTemp;
        this->outTemp = outTemp;
        this->humidity = humidity;
        this->LeftDoor = LeftDoor;
        this->RightDoor = RightDoor;
        this->isInsert = false;
        this->user = 1;
    }

    Transfer() {
        this->id = ESP32_ID;
        this->inTemp = 0.0;
        this->outTemp = 0.0;
        this->humidity = 0.0;
        this->LeftDoor = false;
        this->RightDoor = false;
        this->isInsert = false;
        this->user = 1;
    }

    double getInTemp() const {
        return inTemp;
    }

    void setInTemp(double inTemp) {
        this->inTemp = inTemp;
    }

    double getOutTemp() const {
        return outTemp;
    }

    void setOutTemp(double outTemp) {
        this->outTemp = outTemp;
    }

    double getHumidity() const {
        return humidity;
    }

    void setHumidity(double humidity) {
        this->humidity = humidity;
    }

    bool isLeftDoorOpen() const {
        return LeftDoor;
    }

    void setLeftDoor(bool LeftDoor) {
        this->LeftDoor = LeftDoor;
    }

    bool isRightDoorOpen() const {
        return RightDoor;
    }

    void setRightDoor(bool RightDoor) {
        this->RightDoor = RightDoor;
    }

    bool getIsInsert() const {
        return isInsert;
    }

    void setIsInsert(bool isInsert) {
        this->isInsert = isInsert;
    }

    String toString() const {
        return "{ \"id\": \"" + id + "\"" +
               ", \"inTemp\": " + String(inTemp) +
               ", \"outTemp\": " + String(outTemp) +
               ", \"humidity\": " + String(humidity) +
               ", \"LeftDoor\": " + (LeftDoor ? "true" : "false") +
               ", \"RightDoor\": " + (RightDoor ? "true" : "false") +
               ", \"isInsert\": " + (isInsert ? "true" : "false") +
               ", \"user\": " + user +
               " }";
    }

    bool sendToServer() {
        String data = this->toString();
        // Serial.println(data);
        return webSocketSendString(data);
    }
};
