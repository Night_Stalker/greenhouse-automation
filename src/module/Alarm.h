#pragma once
#include "define.h"

class Alarm {
  private:
    bool alarm_1;
    bool alarm_2;
    bool alarm_3;

  public:
    Alarm(bool alarm_1, bool alarm_2, bool alarm_3) {
        this->alarm_1 = alarm_1;
        this->alarm_2 = alarm_2;
        this->alarm_3 = alarm_3;
    }

    Alarm() {
        this->alarm_1 = false;
        this->alarm_2 = false;
        this->alarm_3 = false;
    }

    // Getter methods
    bool getAlarm1() const {
        return alarm_1;
    }
    bool getAlarm2() const {
        return alarm_2;
    }
    bool getAlarm3() const {
        return alarm_3;
    }

    // Setter methods
    void setAlarm1(bool a1) {
        this->alarm_1 = a1;
    }
    void setAlarm2(bool a2) {
        this->alarm_2 = a2;
    }
    void setAlarm3(bool a3) {
        this->alarm_3 = a3;
    }

    String toString() const {
        String result = "{";
        result += "\"alarm_1\": " + String(alarm_1 ? "true" : "false") + ", ";
        result += "\"alarm_2\": " + String(alarm_2 ? "true" : "false") + ", ";
        result += "\"alarm_3\": " + String(alarm_3 ? "true" : "false");
        result += "}";
        return result;
    }

    bool sendToServer() {
        String data = this->toString();
        return webSocketSendString(data);
    }
};
