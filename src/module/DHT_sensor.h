#pragma once
#include "define.h"

class DHT_sensor {
  private:
    float humidity;
    float temperature;

  public:
    DHT_sensor(double humidity, double temperature) {
        this->humidity = humidity;
        this->temperature = temperature;
    }

    float getHumidity() const {
        return humidity;
    }

    void setHumidity(float humidity) {
        this->humidity = humidity;
    }

    float getTemperature() const {
        return temperature;
    }

    void setTemperature(float temperature) {
        this->temperature = temperature;
    }
};
