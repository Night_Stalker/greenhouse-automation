#include "define.h"

WebSocketsClient webSocket;
bool websocketConnection = false;
bool isAutoFan = false;
bool leftFan = false;
bool rightFan = false;

bool webSocketSendString(String string) {
    if (websocketConnection) {
        webSocket.sendTXT(string) ? Serial.println("Sent message :" + string) : Serial.println("Failed to send.");
        return true;
    } else {
        Serial.println(SOCKED_DISCONNECTED);
        return false;
    }
}

void webSocketEvent(WStype_t type, uint8_t *payload, size_t length) {
    switch (type) {
    case WStype_DISCONNECTED:
        Serial.println(WS_DISCONNECTED);
        digitalWrite(LED_PIN_WEBSOCKET, LOW);
        websocketConnection = false;
        break;
    case WStype_CONNECTED:
        Serial.println(WS_CONNECTED);
        digitalWrite(LED_PIN_WEBSOCKET, HIGH);
        websocketConnection = true;
        break;
    case WStype_TEXT:

        DynamicJsonDocument doc(1024);
        DeserializationError error = deserializeJson(doc, (char *)payload);
        if (error) {
            Serial.println(error.c_str());
            break;
        }

        if (doc["id"] == ESP32_ID && doc["from"] == "webserver") {
            Serial.print(WS_RECEIVE);
            isAutoFan = doc["garUdirdlaga"].as<bool>();
            leftFan = doc["garahSens"].as<bool>();
            rightFan = doc["orohSens"].as<bool>();
            Serial.println((char *)payload);
        }

        break;
    }
}

void configWebSocket() {
    delay(500);
    webSocket.begin(WEB_SOCKET_IPADDRESS, WEB_SOCKET_PORT, WEB_SOCKET_API);
    // webSocket.setExtraHeaders("Accept-Encoding: gzip\r\n");
    // webSocket.setExtraHeaders("Cache-Control: no-cache\r\nAccept-Encoding: gzip\r\n");
    webSocket.onEvent(webSocketEvent);
    webSocket.setReconnectInterval(WEB_SOCKET_RECONNECT_TIME);
}

void webSocketLoop() {
    webSocket.loop();
}
