#pragma once

#define BAUDRATE 921600
#define DS_TEMP_PIN 4
#define ESP32_ID "73f23661-68a5-40f0-b099-1c49653dbb0b"

// Web Socket Config
#define WEB_SOCKET_IPADDRESS "104.197.225.246"
// #define WEB_SOCKET_IPADDRESS "192.168.0.235"
#define WEB_SOCKET_PORT 1880
#define WEB_SOCKET_API "/ws/receive"
#define WEB_SOCKET_RECONNECT_TIME 5000
#define LED_PIN_WEBSOCKET LED_BUILTIN

// WIFI
#define Wifi_SSID "iO Tech 2 2.4GHZ"
#define Wifi_Password "io123456"
#define Wifi_SSID_1 "test123"
#define Wifi_Password_1 "99220101"
#define Wifi_SSID_2 "bayndalai"
#define Wifi_Password_2 "12345679"

// Timers //second
#define WDT_duration 10
#define WiFi_duration 5
#define Insert_duration 10

// dht sensor 
#define DHTPIN 0     
#define DHTTYPE DHT11   

// door pin
#define left_door_pin 18     
#define right_door_pin 19   

// fan pin
#define left_fan 25
#define right_fan 33  

// Constant Strings
#define WS_DISCONNECTED "Client disconnected"
#define WS_CONNECTED "Client connected"
#define WS_RECEIVE "Text Message Received: "
#define WIFI_LOST "WiFi connection lost!"
#define SOCKED_DISCONNECTED "Web socket disconnected. Failed to send information to server"