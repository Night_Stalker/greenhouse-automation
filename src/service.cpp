#include "define.h"
#include "module/Alarm.h"
#include "module/Transfer.h"

OneWire oneWire(DS_TEMP_PIN);
DallasTemperature ds_sensor(&oneWire);
DHT dht(DHTPIN, DHTTYPE);
bool isAuto = false;
bool isSendData = false;
bool wifi_interrupt = false;
bool timer_alarm3 = false;
float ds_sensor_value;

bool before_alarm_1 = false;
bool before_alarm_2 = false;

void syncSensor() {
    ds_sensor.begin();
    dht.begin();
}

bool getAndSendData(bool state) {
    ds_sensor_value = getDsSensorValue();
    DHT_sensor dht_sensor = getDataFromDht();

    Transfer transfer;
    transfer.setHumidity(dht_sensor.getHumidity());
    transfer.setInTemp(dht_sensor.getTemperature());
    transfer.setIsInsert(state);

    if (!isnan(ds_sensor_value)) {
        transfer.setOutTemp(ds_sensor_value);
    }

    int door_1 = digitalRead(left_door_pin);
    int door_2 = digitalRead(right_door_pin);
    transfer.setLeftDoor(door_1 == HIGH);
    transfer.setRightDoor(door_2 == HIGH);

    // Serial.println("data: " + transfer.toString());

    return transfer.sendToServer();
}

void checkAlarm() {
    bool is_sendAlarm = false;
    Alarm alarm;
    if (ds_sensor_value >= 28) {
        alarm.setAlarm1(true);
        if (alarm.getAlarm1() != before_alarm_1) {
            Serial.println("in state - 1");
            before_alarm_1 = alarm.getAlarm1();
            is_sendAlarm = true;
        }
    } else if (ds_sensor_value <= 20) {
        alarm.setAlarm2(true);
        if (alarm.getAlarm2() != before_alarm_2) {
            Serial.println("in state - 2");
            before_alarm_2 = alarm.getAlarm2();
            is_sendAlarm = true;
        }
    } else {
        alarm.setAlarm1(false);
        alarm.setAlarm2(false);
        if (alarm.getAlarm1() != before_alarm_1) {
            Serial.println("in state - 3");
            before_alarm_1 = alarm.getAlarm1();
            is_sendAlarm = true;
        }
        if (alarm.getAlarm2() != before_alarm_2) {
            Serial.println("in state - 4");
            before_alarm_2 = alarm.getAlarm2();
            is_sendAlarm = true;
        }
    }

    // if (timer_alarm3) {
    //     timer_alarm3 = false;
    //     Serial.println("alarm-3");
    //     alarm.setAlarm3(true);
    //     // is_sendAlarm = true;
    //     // timerAlarmDisable(timer1);
    // }

    if (is_sendAlarm) {
        alarm.sendToServer();
    }
}

float getDsSensorValue() {
    ds_sensor.requestTemperatures();
    return ds_sensor.getTempCByIndex(0);
}

DHT_sensor getDataFromDht() {
    delay(2000);
    float humidity = dht.readHumidity();
    float temperature = dht.readTemperature();

    if (isnan(humidity)) {
        humidity = 0.0;
    }

    if (isnan(temperature)) {
        temperature = 0.0;
    }

    DHT_sensor dht_sensor(humidity, temperature);
    return dht_sensor;
}