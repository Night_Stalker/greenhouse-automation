#include "define.h"

unsigned long previousMillis = 0;
const long interval = 10000;
unsigned long currentMillis;
bool state = false;
int door_1;
int door_2;

void setup() {
    Serial.begin(BAUDRATE);
    configPins();
    digitalWrite(LED_PIN_WEBSOCKET, LOW);
    initWifi();
    configWebSocket();
    esp_task_wdt_init(WDT_duration, true);
    initTimer();
    syncSensor();
    ds_sensor_value = getDsSensorValue();
}

void loop() {

    door_1 = digitalRead(left_door_pin);
    door_2 = digitalRead(right_door_pin);

    if (door_1 || door_2) {
        timerAlarmEnable(timer1);
    }

    currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
        previousMillis = currentMillis;
        state = true;
    }

    if (isAutoFan) {
        digitalWrite(left_fan, leftFan ? HIGH : LOW);
        digitalWrite(right_fan, rightFan ? HIGH : LOW);
    } else {
        if (ds_sensor_value >= 24) {
            digitalWrite(left_fan, HIGH);
            digitalWrite(right_fan, HIGH);
        }

        if (ds_sensor_value <= 20) {
            digitalWrite(left_fan, LOW);
            digitalWrite(right_fan, LOW);
        }
    }

    checkAlarm();

    if (wifi_interrupt) {
        Serial.println(WIFI_LOST);
        tryToConnectWifi();
        wifi_interrupt = false;
    }

    if (isSendData) {
        getAndSendData(state);
        isSendData = false;
        state = false;
    }

    webSocketLoop();
    esp_task_wdt_reset();
}