#pragma once

#include "DHT.h"
#include "constant.h"
#include "esp_system.h"
#include "esp_task_wdt.h"
#include <Adafruit_Sensor.h>
#include <Arduino.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include <WebSocketsClient.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include "module/DHT_sensor.h"
#include <ArduinoJson.h>

extern DHT dht;
extern OneWire oneWire;
extern DallasTemperature ds_sensor;
extern WebSocketsClient webSocket;
extern bool websocketConnection;
extern WiFiMulti wifiMulti;
extern hw_timer_t *timer;
extern hw_timer_t *timer1;
extern bool isAutoFan;
extern bool leftFan;
extern bool rightFan;
extern bool isSendData;
extern bool wifi_interrupt;
extern float ds_sensor_value;
extern bool timer_alarm3;
// extern volatile bool isSendData;

// function prototype
void syncSensor();
float getDsSensorValue();
DHT_sensor getDataFromDht();
void initWifi();
void webSocketLoop();
void configWebSocket();
void configPins();
void initTimer();
void tryToConnectWifi();
bool webSocketSendString(String string);
bool getAndSendData(bool state);
void checkAlarm();

// interrupt
void IRAM_ATTR onTimerWifi();
void IRAM_ATTR onTimer5min();

