#include "define.h"

WiFiMulti wifiMulti;
hw_timer_t *timer = NULL;
hw_timer_t *timer1 = NULL;

void tryToConnectWifi() {
    Serial.println("Connecting Wifi...");
    if (wifiMulti.run() == WL_CONNECTED) {
        Serial.println("");
        Serial.print("WiFi connected");
        Serial.println(WiFi.SSID());
        Serial.print("IP address: ");
        Serial.println(WiFi.localIP());
        Serial.println("");
    } else {
        Serial.println("Failed to connect to WiFi.");
    }
    delay(100);
}

void initWifi() {
    WiFi.mode(WIFI_STA);
    wifiMulti.addAP(Wifi_SSID, Wifi_Password);
    wifiMulti.addAP(Wifi_SSID_1, Wifi_Password_1);
    wifiMulti.addAP(Wifi_SSID_2, Wifi_Password_2);

    int n = WiFi.scanNetworks();
    Serial.println("scan done");
    if (n == 0) {
        Serial.println("no networks found");
    } else {
        Serial.print(n);
        Serial.println(" networks found");
        for (int i = 0; i < n; ++i) {
            Serial.print(i + 1);
            Serial.print(": ");
            Serial.print(WiFi.SSID(i));
            Serial.print(" (");
            Serial.print(WiFi.RSSI(i));
            Serial.print(")");
            Serial.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN) ? " " : "*");
            delay(10);
        }
    }
    tryToConnectWifi();
}

void configPins() {
    pinMode(LED_PIN_WEBSOCKET, OUTPUT);
    pinMode(left_door_pin, INPUT);
    pinMode(right_door_pin, INPUT);
    pinMode(left_fan, OUTPUT);
    pinMode(right_fan, OUTPUT);
}

void initTimer() {
    timer = timerBegin(0, 80, true);
    timerAttachInterrupt(timer, &onTimerWifi, true);
    timerAlarmWrite(timer, WiFi_duration * 1000000, true);
    timerAlarmEnable(timer);

    // timer1 = timerBegin(1, 80, true);
    // timerAttachInterrupt(timer1, &onTimer5min, true);
    // timerAlarmWrite(timer1, 5 * 1000000, true);
    // timerAlarmWrite(timer1, 5 * 60 * 1000000, true);
}
